![](https://img.shields.io/badge/Microverse-blueviolet)
# Math magicians app
If you like Math and problem solving then this website is for you. Math-magicians app is a SPA(single-page-app)built with React.js It allows users to solve different problems use the in-built calculator and get motivated by the quotes popping up in there.

# Screenshot
![Screenshot (25)](https://user-images.githubusercontent.com/90222110/154003217-c241efc5-e25d-445d-b595-3eb0addc12ad.png)


# Built with
- React.js
- HTML
- CSS

# Other tools
- Webpack and Babel compiler


# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## installation

This project has a lot of dependencies. In order to run on your machine very well, you need to install them by typing in the terminal

`npm install`

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)

# Authors

👤 **Bertrand Mutangana Ishimwe**

- GitHub: [Bertrand Mutangana Ishimwe](https://github.com/BertrandConxy)
- Twitter: [Bertrand Mutangana Ishimwe](https://twitter.com/BertrandMutanga)
- LinkedIn: [Bertrand Mutangana Ishimwe](https://www.linkedin.com/in/bertrand-mutangana-024905220/)

## 🤝 Contributing

Contributions, issues, and feature requests are welcome!

Feel free to check the [issues page](https://github.com/BertrandConxy/Math-magicians-app/issues).

## Show your support

Give a ⭐️ if you like this project!

## Acknowledgments

- Thanks to everyone who helped  me.
